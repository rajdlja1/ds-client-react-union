import loadTrainsetOverviewWidget from '../../widgets/TrainsetOverview/trainsetoverview.widget';
import loadTrainsetDetailWidget from '../../widgets/TrainsetDetail/trainsetdetail.widget';
import loadRouteDetailWidget from '../../widgets/RouteDetail/routedetail.widget';
import loadRouteOverviewWidget from '../../widgets/RouteOverview/routeOverview.widget';
import loadTrainsetEditorWidget from '../../widgets/TrainsetEditor/editor.widget';

export default [
	{
		path: 'trainsetOverview',
		getComponent: done => {
			loadTrainsetOverviewWidget(mod => done(mod.default));
		},
	},
	{
		path: 'routeOverview',
		getComponent: done => {
			loadRouteOverviewWidget(mod => done(mod.default));
		},
	},
	{
		path: 'trainsetDetail',
		getComponent: done => {
			loadTrainsetDetailWidget(mod => done(mod.default));
		},
	},
	{
		path: 'routeDetail',
		getComponent: done => {
			loadRouteDetailWidget(mod => done(mod.default));
		},
	},
	{
		path: 'trainsetEditor',
		getComponent: done => {
			loadTrainsetEditorWidget(mod => done(mod.default));
		},
	},
];
