import React from 'react';
import { FormattedMessage, FormattedDate, FormattedTime } from 'react-intl';

function renderVehicle(vehicle, setId) {
	return <span key={`${setId}-${vehicle.id}-${vehicle.description}`}>{vehicle.name}</span>;
}

/* TODO redirect to another liferay portlet */
function handleEditButton(id) {
	console.log(`Edited Button Clicked: ${id}`);
}

function renderRide(key, ride) {
	const rideId = ride.id;
	const vehicles = [];

	if (ride.vehicles != null) {
		ride.vehicles.forEach(function(vehicle) {
			vehicles.push(renderVehicle(vehicle, rideId));
		});
	}

	return (
		<tr key={ride.id}>
			<td>
				{vehicles.length > 0 ? (
					vehicles.map(t => t).reduce((prev, curr) => [prev, ', ', curr])
				) : (
					<FormattedMessage id="trainsetDetail.notHaveConfiguration" />
				)}
			</td>
			<td>
				{ride.driver !== null ? (
					`${ride.driver.firstname} ${ride.driver.lastname}`
				) : (
					<FormattedMessage id="trainsetDetail.notAssigned" />
				)}
			</td>
			<td>{ride.driver !== null ? ride.driver.phoneNunber : ''}</td>
			<td>{ride.routeSection.from.location}</td>
			<td>
				<FormattedDate value={ride.routeSection.departureTime} />{' '}
				<FormattedTime value={ride.routeSection.departureTime} />
			</td>
			<td>{ride.routeSection.to.location}</td>
			<td>
				<FormattedDate value={ride.routeSection.arrivalTime} />{' '}
				<FormattedTime value={ride.routeSection.arrivalTime} />
			</td>
			<td>
				<div className="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
					<div className="btn-group mr-2" role="group" aria-label="First group">
						<button className="btn btn-info" onClick={() => handleEditButton(ride.id)}>
							<FormattedMessage id="trainsetDetail.editRouteSectionLabel" />
						</button>
					</div>
					<div className="btn-group mr-2" role="group" aria-label="First group" />
				</div>
			</td>
		</tr>
	);
}

export default renderRide;
