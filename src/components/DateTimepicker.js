import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { injectIntl, intlShape } from 'react-intl';

import DateTimePicker from 'material-ui-datetimepicker';
import DatePickerDialog from 'material-ui/DatePicker/DatePickerDialog';
import TimePickerDialog from 'material-ui/TimePicker/TimePickerDialog';

class DateTimepicker extends React.Component {
	constructor(props) {
		super(props);
		const today = new Date();
		const minDate = new Date();
		const maxDate = new Date();
		minDate.setFullYear(minDate.getFullYear() - 1);
		minDate.setHours(0, 0, 0, 0);
		maxDate.setFullYear(maxDate.getFullYear() + 1);
		maxDate.setHours(0, 0, 0, 0);

		this.state = {
			minDate,
			maxDate,
			today,
		};
	}

	handleChange = dateTime => {
		this.props.dispatch(this.props.handler(dateTime));
	};

	render() {
		const language = navigator.language.split(/[-_]/)[0]; // language without region code\
		const { intl } = this.props;
		const value = `${intl.formatDate(this.props.date)}	${intl.formatTime(this.props.date)}`;
		return (
			<DateTimePicker
				onChange={this.handleChange}
				showCurrentDateByDefault
				value={value}
				DateTimeFormat={Intl.DateTimeFormat}
				locale={language}
				DatePicker={DatePickerDialog}
				TimePicker={TimePickerDialog}
				floatingLabelText={this.props.label}
				minDate={this.state.minDate}
				maxDate={this.state.maxDate}
			/>
		);
	}
}

DateTimepicker.propTypes = {
	date: PropTypes.string,
	dispatch: PropTypes.func.isRequired,
	handler: PropTypes.func,
	intl: intlShape.isRequired,
	label: PropTypes.string,
};

const mapDispatchToProps = () => {
	return {};
};

export default connect(mapDispatchToProps)(injectIntl(DateTimepicker));
