import React from 'react';
import { connect } from 'react-redux';
import DatePicker from 'material-ui/DatePicker';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { selectDate } from '../actions';


class Datepicker extends React.Component {
	constructor(props) {
		super(props);
		const today = new Date();
		const minDate = new Date();
		const maxDate = new Date();
		minDate.setFullYear(minDate.getFullYear() - 1);
		minDate.setHours(0, 0, 0, 0);
		maxDate.setFullYear(maxDate.getFullYear() + 1);
		maxDate.setHours(0, 0, 0, 0);

		this.state = {
			minDate,
			maxDate,
			today,
		};
	}

	handleChange = (event, date) => {
		this.props.dispatch(selectDate(date));
	};

	render() {
		return (
			<DatePicker
				onChange={this.handleChange}
				floatingLabelText={<FormattedMessage id="form.label.chooseDate" />}
				minDate={this.state.minDate}
				maxDate={this.state.maxDate}
				value={this.state.today}
				autoOk
			/>
		);
	}
}

Datepicker.propTypes = {
	dispatch: PropTypes.func.isRequired,
};

const mapDispatchToProps = () => {
	return {};
};

export default connect(mapDispatchToProps)(Datepicker);
