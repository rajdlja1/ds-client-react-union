import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import Datepicker from '../../../components/Datepicker';
import getFormattedDate from '../../../components/DateFormatter';
import { invalidateForm, changeCheckBox, reloadTrainsetsIfNeeded } from '../actions';

const React = require('react');

const DaysEnum = Object.freeze({
	ON_THE_WAY: 'ON_THE_WAY',
	ARRIVED: 'ARRIVED',
	WILL_DEPARTURE: 'WILL_DEPARTURE',
	TODAY: 'TODAY',
	NOTHING_SELECTED: 'NOTHING_SELECTED',
});

class SearchForm extends React.Component {
	static ON_THE_WAY = 'NACESTE';
	static ARRIVAL = 'PRIJEZD';
	static DEPARTURE = 'ODJEZD';

	static SUBMIT_FORM_LABEL = 'Zobrazit';

	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleCheckboxState = val => {
		if (val === this.props.checkboxValue) {
			this.props.dispatch(changeCheckBox(DaysEnum.NOTHING_SELECTED));
		} else {
			this.props.dispatch(changeCheckBox(val));
		}
	};

	handleDatePicker(datePicker) {
		this.setState({ date: datePicker });
	}

	handleSubmit(event) {
		event.preventDefault();

		let time = SearchForm.DEPARTURE;
		const startOfDate = new Date();
		startOfDate.setHours(0, 0, 0, 0);
		const endOfDate = new Date();
		endOfDate.setHours(23, 59, 0, 0);
		const now = new Date();

		// Default Option is TODAY
		let since = getFormattedDate(startOfDate);
		let until = getFormattedDate(endOfDate);

		// Today Option
		if (this.props.checkboxValue === DaysEnum.TODAY) {
			since = getFormattedDate(startOfDate);
			until = getFormattedDate(endOfDate);
		} else if (this.props.checkboxValue === DaysEnum.ON_THE_WAY) {
			// Now
			since = getFormattedDate(now);
			until = getFormattedDate(now);
			time = SearchForm.ON_THE_WAY;
		} else if (this.props.checkboxValue === DaysEnum.ARRIVED) {
			// Arrived Today
			since = getFormattedDate(startOfDate);
			until = getFormattedDate(now);
			time = SearchForm.ARRIVAL;
		} else if (this.props.checkboxValue === DaysEnum.WILL_DEPARTURE) {
			// Will Departure Today
			since = getFormattedDate(now);
			until = getFormattedDate(endOfDate);
			time = SearchForm.DEPARTURE;
		} else if (this.props.selectedDate != null) {
			// Check date from datepicker
			const startOfSubmDate = new Date(this.props.selectedDate.getTime());
			startOfSubmDate.setHours(0, 0, 0, 0);
			const endOfSubmDate = new Date(this.props.selectedDate.getTime());
			endOfSubmDate.setHours(23, 59, 0, 0);
			since = getFormattedDate(startOfSubmDate);
			until = getFormattedDate(endOfSubmDate);
		}

		this.props.dispatch(invalidateForm());
		this.props.dispatch(reloadTrainsetsIfNeeded(since, until, time));
	}

	render() {
		return (
			<form onSubmit={this.handleSubmit}>
				<div className="form-group">
					<Datepicker />
				</div>

				<div className="form-group">
					<div className="form-check form-check-inline">
						<input
							className="form-check-input"
							type="checkbox"
							id="todayCheckbox"
							value="todayOption"
							checked={this.props.checkboxValue === DaysEnum.TODAY}
							onChange={() => this.handleCheckboxState(DaysEnum.TODAY)}
						/>
						<label className="form-check-label" htmlFor="todayCheckbox">
							<FormattedMessage id="form.label.today" />
						</label>
					</div>
					<div className="form-check form-check-inline">
						<input
							className="form-check-input"
							type="checkbox"
							id="nowCheckbox"
							value="nowOption"
							checked={this.props.checkboxValue === DaysEnum.ON_THE_WAY}
							onChange={() => this.handleCheckboxState(DaysEnum.ON_THE_WAY)}
						/>
						<label className="form-check-label" htmlFor="nowCheckbox">
							<FormattedMessage id="form.label.onTheWay" />
						</label>
					</div>
					<div className="form-check form-check-inline">
						<input
							className="form-check-input"
							type="checkbox"
							id="arrivedCheckbox"
							value="arrivedOption"
							checked={this.props.checkboxValue === DaysEnum.ARRIVED}
							onChange={() => this.handleCheckboxState(DaysEnum.ARRIVED)}
						/>
						<label className="form-check-label" htmlFor="arrivedCheckbox">
							<FormattedMessage id="form.label.arrived" />
						</label>
					</div>
					<div className="form-check form-check-inline">
						<input
							className="form-check-input"
							type="checkbox"
							id="willDepartureCheckbox"
							value="willDepartureOption"
							checked={this.props.checkboxValue === DaysEnum.WILL_DEPARTURE}
							onChange={() => this.handleCheckboxState(DaysEnum.WILL_DEPARTURE)}
						/>
						<label className="form-check-label" htmlFor="willDepartureCheckbox">
							<FormattedMessage id="form.label.willDeparture" />
						</label>
					</div>

					<input type="submit" value={SearchForm.SUBMIT_FORM_LABEL} className="btn btn-primary" />
				</div>
			</form>
		);
	}
}

SearchForm.propTypes = {
	checkboxValue: PropTypes.string,
	dispatch: PropTypes.func,
	handleFilterTrainsets: PropTypes.func,
	selectedDate: PropTypes.instanceOf(Date),
};

const mapStateToProps = state => {
	return {
		checkboxValue: state.changedCheckboxValue,
		selectedDate: state.selectedDate,
	};
};

export default connect(mapStateToProps)(SearchForm);
