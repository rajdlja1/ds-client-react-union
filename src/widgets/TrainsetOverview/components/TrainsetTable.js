/* eslint-disable react/prefer-stateless-function */
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import Trainset from './Trainset';

const React = require('react');

class TrainsetTable extends React.Component {
	render() {
		const rows = [];
		Array.prototype.forEach.call(this.props.trainsets, trainset => {
			rows.push(<Trainset key={trainset.id} trainset={trainset} />);
		});

		return (
			<div className="container">
				<table className="table table-striped">
					<thead>
						<tr>
							<th>
								<FormattedMessage id="trainsetTable.trainsets" />
							</th>
							<th>
								<FormattedMessage id="trainsetTable.cars" />
							</th>
							<th>
								<FormattedMessage id="trainsetTable.actions" />
							</th>
						</tr>
					</thead>
					<tbody>{rows}</tbody>
				</table>
			</div>
		);
	}
}

TrainsetTable.propTypes = {
	showRoutes: PropTypes.func,
	trainsets: PropTypes.arrayOf(
		PropTypes.shape({
			name: PropTypes.string,
			description: PropTypes.string,
		})
	),
};

export default TrainsetTable;
