export const REQUEST_POSTS = 'REQUEST_POSTS';
export const RECEIVE_POSTS = 'RECEIVE_POSTS';
export const INVALIDATE_FORM = 'INVALIDATE_FORM';
export const CHANGE_CHECKBOX_VALUE = 'CHANGE_CHECKBOX_VALUE';

export const changeCheckBox = checkboxValue => ({
	type: CHANGE_CHECKBOX_VALUE,
	checkboxValue,
});

const fetchPosts = () => dispatch => {
	dispatch(requestPosts());
	return fetch(
		'http://localhost:8080/soupravy/hledej?od=2017-01-01-12:59&do=2018-08-08-12:59&cas=prijezd'
	)
		.then(response => response.json())
		.then(json => dispatch(receivePosts(json)));
};

const reloadItems = (since, until, time) => dispatch => {
	dispatch(requestPosts());
	return fetch(`http://localhost:8080/soupravy/hledej?od=${since}&do=${until}&cas=${time}`)
		.then(response => response.json())
		.then(json => dispatch(receivePosts(json)));
};

export const requestPosts = () => ({
	type: REQUEST_POSTS,
});

export const receivePosts = json => ({
	type: RECEIVE_POSTS,
	posts: json.map(child => child),
	receivedAt: Date.now(),
});

export const invalidateForm = () => ({
	type: INVALIDATE_FORM,
});

const shouldFetchPosts = state => {
	const posts = state.filteredPosts;
	if (!posts) {
		return true;
	}
	if (posts.isFetching) {
		return false;
	}
	return posts.didInvalidate;
};

export const fetchTrainsetsIfNeeded = () => (dispatch, getState) => {
	if (shouldFetchPosts(getState())) {
		return dispatch(fetchPosts());
	}
};

export const reloadTrainsetsIfNeeded = (since, until, time) => (dispatch, getState) => {
	if (shouldFetchPosts(getState())) {
		return dispatch(reloadItems(since, until, time));
	}
};
