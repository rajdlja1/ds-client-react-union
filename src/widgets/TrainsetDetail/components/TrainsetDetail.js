import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import renderTrainTable from './TrainTable';
import SearchForm from './SearchForm';

const React = require('react');

function renderTrainset(trainset) {
	return (
		<div className="container">
			<MuiThemeProvider>
				<SearchForm />
			</MuiThemeProvider>

			<h2>Jmeno soupravy: {trainset.name}</h2>
			{renderTrainTable(trainset)}
		</div>
	);
}

export default renderTrainset;
