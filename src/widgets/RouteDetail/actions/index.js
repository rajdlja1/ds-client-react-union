/* TODO Change hardcoded URLs */

export const REQUEST_ROUTE = 'REQUEST_ROUTE';
export const RECEIVE_ROUTE = 'RECEIVE_ROUTE';
export const SHOW_ROUTE_SECTIONS_CHANGED = 'SHOW_ROUTE_SECTIONS_CHANGED';
export const SELECT_FROM_DATE = 'SELECT_FROM_DATE';
export const SELECT_TO_DATE = 'SELECT_TO_DATE';
export const INVALIDATE_FORM = 'INVALIDATE_FORM';
export const CHECKBOX_CHANGE = 'CHECKBOX_CHANGE';

export const showRouteSectionsChanged = train => ({
	type: SHOW_ROUTE_SECTIONS_CHANGED,
	train,
});

export const selectFromDate = dateTime => ({
	type: SELECT_FROM_DATE,
	dateTime,
});

export const checkBoxChanged = () => ({
	type: CHECKBOX_CHANGE,
});

export const selectToDate = dateTime => ({
	type: SELECT_TO_DATE,
	dateTime,
});

export const fetchRoute = () => {
	return dispatch => {
		dispatch(requestRoute());
		return fetch('http://localhost:8080/spoje/0')
			.then(response => response.json())
			.then(json => dispatch(receiveRoute(json)));
	};
};

export const requestRoute = () => ({
	type: REQUEST_ROUTE,
});

export const receiveRoute = json => ({
	type: RECEIVE_ROUTE,
	trainset: json,
	receivedAt: Date.now(),
});

export const invalidateForm = () => ({
	type: INVALIDATE_FORM,
});

const shouldReloadRoute = state => {
	const posts = state.filteredPosts;
	if (!posts) {
		return true;
	}
	if (posts.isFetching) {
		return false;
	}
	return posts.didInvalidate;
};

export const reloadRouteIfNeeded = (since, until, time) => (dispatch, getState) => {
	if (shouldReloadRoute(getState())) {
		return dispatch(reloadItems(since, until, time));
	}
};

const reloadItems = (since, until, time) => dispatch => {
	dispatch(requestRoute());
	return fetch(`http://localhost:8080/spoje/0/hledej?od=${since}&do=${until}&cas=${time}`)
		.then(response => response.json())
		.then(json => dispatch(receiveRoute(json)));
};
