import React from 'react';
import { FormattedMessage } from 'react-intl';
import Train from './Train';

function renderTrainTable(obj) {
	const rows = [];
	Array.prototype.forEach.call(obj.trains, train => {
		rows.push(<Train key={train.id} train={train} />);
	});

	return (
		<div className="container">
			<table className="table table-striped">
				<thead>
					<tr>
						<th>
							<FormattedMessage id="trainsetDetail.cars" />
						</th>
						<th>
							<FormattedMessage id="trainsetDetail.driver" />
						</th>
						<th>
							<FormattedMessage id="trainsetDetail.phoneNumber" />
						</th>
						<th>
							<FormattedMessage id="trainsetDetail.stationOfDeparture" />
						</th>
						<th>
							<FormattedMessage id="trainsetDetail.timeOfDeparture" />
						</th>
						<th>
							<FormattedMessage id="trainsetDetail.stationOfArrival" />
						</th>
						<th>
							<FormattedMessage id="trainsetDetail.timeOfArrival" />
						</th>
						<th>
							<FormattedMessage id="trainsetDetail.actions" />
						</th>
					</tr>
				</thead>
				{rows}
			</table>
		</div>
	);
}

export default renderTrainTable;
