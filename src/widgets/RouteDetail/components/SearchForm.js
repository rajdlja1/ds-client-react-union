import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';
import DateTimepicker from '../../../components/DateTimepicker';
import getFormattedDate from '../../../components/DateFormatter';

import {
	invalidateForm,
	reloadRouteIfNeeded,
	selectFromDate,
	selectToDate,
	checkBoxChanged,
} from '../actions';

class SearchForm extends React.Component {
	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleCheckboxState = () => {
		this.props.dispatch(checkBoxChanged());
	};

	handleSubmit(event) {
		event.preventDefault();

		const { intl } = this.props;
		const time = intl.formatMessage({ id: 'searchForm.time.departure' });
		const startOfDate = new Date();
		startOfDate.setHours(0, 0, 0, 0);
		const endOfDate = new Date();
		endOfDate.setHours(23, 59, 0, 0);

		// Default Option is TODAY
		let since = getFormattedDate(startOfDate);
		let until = getFormattedDate(endOfDate);

		console.log('here');
		console.log(this.props.checkbox);

		// Today Option
		if (this.props.checkbox === true) {
			console.log('ok');
			since = getFormattedDate(startOfDate);
			until = getFormattedDate(endOfDate);
		} else if (this.props.selectedFromDate != null && this.props.selectedToDate != null) {
			// Check date from datepicker
			const startOfSubmDate = new Date(this.props.selectedFromDate.getTime());
			const endOfSubmDate = new Date(this.props.selectedToDate.getTime());
			since = getFormattedDate(startOfSubmDate);
			until = getFormattedDate(endOfSubmDate);
		}

		this.props.dispatch(invalidateForm());
		this.props.dispatch(reloadRouteIfNeeded(since, until, time));
	}

	render() {
		const { intl } = this.props;
		return (
			<form onSubmit={this.handleSubmit}>
				<div className="form-group">
					<DateTimepicker
						handler={dateTime => selectFromDate(dateTime)}
						date={this.props.selectedFromDate}
						label={intl.formatMessage({ id: 'searchForm.from.label' })}
					/>
					<DateTimepicker
						handler={dateTime => selectToDate(dateTime)}
						date={this.props.selectedToDate}
						label={intl.formatMessage({ id: 'searchForm.to.label' })}
					/>
				</div>

				<div className="form-group">
					<div className="form-check form-check-inline">
						<input
							className="form-check-input"
							type="checkbox"
							id="todayCheckbox"
							value="todayOption"
							checked={this.props.checkbox}
							onChange={() => this.handleCheckboxState()}
						/>
						<label className="form-check-label" htmlFor="todayCheckbox">
							{intl.formatMessage({ id: 'searchForm.today.label' })}
						</label>
					</div>

					<input
						type="submit"
						value={intl.formatMessage({ id: 'searchForm.submit' })}
						className="btn btn-primary"
					/>
				</div>
			</form>
		);
	}
}

SearchForm.propTypes = {
	checkbox: PropTypes.bool,
	dispatch: PropTypes.func,
	handleFilterTrainsets: PropTypes.func,
	intl: intlShape.isRequired,
	selectedFromDate: PropTypes.instanceOf(Date),
	selectedToDate: PropTypes.instanceOf(Date),
};

const mapStateToProps = state => {
	return {
		selectedFromDate: state.selectedFromDate,
		selectedToDate: state.selectedToDate,
		checkbox: state.todayCheckBox,
	};
};

export default connect(mapStateToProps)(injectIntl(SearchForm));
