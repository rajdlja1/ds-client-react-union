import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { FormattedMessage } from 'react-intl';
import renderTrainTable from './TrainTable';
import SearchForm from './SearchForm';


function renderRoute(route) {
	return (
		<div className="container">
			<MuiThemeProvider>
				<SearchForm />
			</MuiThemeProvider>

			<h2><FormattedMessage id="routeDetail.route" />: {route.from.location} - {route.to.location}</h2>
			{renderTrainTable(route)}
		</div>
	);
}

export default renderRoute;
