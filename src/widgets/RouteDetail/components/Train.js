import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, FormattedDate, FormattedTime, injectIntl, intlShape } from 'react-intl';
import { connect } from 'react-redux';
import { showRouteSectionsChanged } from '../../RouteDetail/actions';
import renderRide from '../../../components/Ride';

function renderVehicle(vehicle, setId) {
	return <span key={`${setId}-${vehicle.id}-${vehicle.description}`}>{vehicle.name}</span>;
}

class Train extends React.Component {
	handleShowRouteSections = () => {
		console.log(this.props.train.showRouteSections);
		this.props.dispatch(showRouteSectionsChanged(this.props.train));
	};

	render() {
		const { intl } = this.props;

		const trainId = this.props.train.id;
		const vehicles = [];
		if (this.props.train.rides != null && this.props.train.rides.length > 0) {
			this.props.train.rides[0].vehicles.forEach(function(vehicle) {
				vehicles.push(renderVehicle(vehicle, trainId));
			});
		}

		const rides = [];

		if (this.props.train.rides != null && this.props.train.rides.length > 0) {
			this.props.train.rides.forEach(function(ride) {
				rides.push(renderRide(`${trainId}-${ride.id}`, ride));
			});
		}

		return (
			<tbody>
			<tr key={this.props.train.id} className="trainRow">
				<td>
					{vehicles.length > 0 ? (
						vehicles.map(t => t).reduce((prev, curr) => [prev, ', ', curr])
					) : (
						<FormattedMessage id="trainsetDetail.notHaveConfiguration" />
					)}
				</td>
				<td>
					{this.props.train.driver !== null ? (
						`${this.props.train.driver.firstname} ${this.props.train.driver.lastname}`
					) : (
						<FormattedMessage id="trainsetDetail.notAssigned" />
					)}
				</td>
				<td>{this.props.train.driver !== null ? this.props.train.driver.phoneNumber : ''}</td>
					<td />
				<td>
					<FormattedDate value={this.props.train.departureTime} />{' '}
					<FormattedTime value={this.props.train.departureTime} />
				</td>
					<td />
				<td>
					<FormattedDate value={this.props.train.arrivalTime} />{' '}
					<FormattedTime value={this.props.train.arrivalTime} />
				</td>
				<td>
					<div className="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
						<div className="btn-group mr-2" role="group" aria-label="First group">
							<button
								className="btn btn-info"
								onClick={() => this.handleShowRouteSections(this.props.train.id)}
							>
								{ intl.formatMessage({ id: 'trainsetDetail.table.action.showRouteSections' }) }
							</button>
						</div>
						<div className="btn-group mr-2" role="group" aria-label="First group" />
					</div>
				</td>
			</tr>
			{this.props.train.showRouteSections !== false && rides}
			</tbody>
		);
	}
}

Train.propTypes = {
	dispatch: PropTypes.func.isRequired,
	intl: intlShape.isRequired,
	train: PropTypes.shape({
		arrivalTime: PropTypes.string,
		departureTime: PropTypes.string,
		trains: PropTypes.arrayOf(
			PropTypes.shape({
				description: PropTypes.string,
				arrivalTime: PropTypes.instanceOf(Date),
				departureTime: PropTypes.instanceOf(Date),
			})
		),
		cars: PropTypes.arrayOf(
			PropTypes.shape({
				name: PropTypes.string,
				description: PropTypes.string,
			})
		),
		name: PropTypes.string,
		id: PropTypes.number,
		showRouteSections: PropTypes.bool,
		rides: PropTypes.array,
		driver: PropTypes.shape({
			id: PropTypes.number,
			firstname: PropTypes.string,
			lastname: PropTypes.string,
			phoneNumber: PropTypes.string,
		}),
		route: PropTypes.shape({
			from: PropTypes.shape({
				location: PropTypes.string,
			}),
			to: PropTypes.shape({
				location: PropTypes.string,
			}),
			departureTime: PropTypes.string,
			arrivalTime: PropTypes.string,
		}),
	}),
};

const mapStateToProps = () => {
	return {};
};

export default connect(mapStateToProps)(injectIntl(Train));
