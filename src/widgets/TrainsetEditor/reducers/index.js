import { combineReducers } from 'redux';
import {
	REQUEST_AVAILABLE_VEHICLES,
	RECEIVE_AVAILABLE_VEHICLES,
	REQUEST_RIDE,
	RECEIVE_RIDE,
	SAVE_TRAINSET_CONFIGURATION,
} from '../actions';

const availableVehiclesMan = (
	state = {
		isFetching: false,
		didInvalidate: false,
		item: null,
	},
	action
) => {
	switch (action.type) {
		case REQUEST_AVAILABLE_VEHICLES:
			return {
				...state,
				isFetching: true,
				didInvalidate: false,
			};
		case RECEIVE_AVAILABLE_VEHICLES:
			return {
				...state,
				isFetching: false,
				didInvalidate: false,
				item: action.availableVehicles,
				lastUpdated: action.receivedAt,
			};
		default:
			return state;
	}
};

const filteredAvailableVehicles = (state = {}, action) => {
	switch (action.type) {
		case RECEIVE_AVAILABLE_VEHICLES:
		case REQUEST_AVAILABLE_VEHICLES:
		default:
			return {
				...state,
				availableVehicles: availableVehiclesMan(state.availableVehicles, action),
			};
	}
};

const rideMan = (
	state = {
		isFetching: false,
		didInvalidate: false,
		item: null,
	},
	action
) => {
	switch (action.type) {
		case REQUEST_RIDE:
			return {
				...state,
				isFetching: true,
				didInvalidate: false,
			};
		case RECEIVE_RIDE:
			return {
				...state,
				isFetching: false,
				didInvalidate: false,
				item: action.ride,
				lastUpdated: action.receivedAt,
			};
		default:
			return state;
	}
};

const filteredRide = (state = {}, action) => {
	switch (action.type) {
		case RECEIVE_RIDE:
		case REQUEST_RIDE:
			return {
				...state,
				ride: rideMan(state.ride, action),
			};
		default:
			return state;
	}
};

const savedTrainsetConfiguration = (state = {}, action) => {
	switch (action.type) {
		case SAVE_TRAINSET_CONFIGURATION:
			return action.vehicles;
		default:
			return state;
	}
};


const rootReducer = combineReducers({
	filteredAvailableVehicles,
	filteredRide,
	savedTrainsetConfiguration,
});

export default rootReducer;
