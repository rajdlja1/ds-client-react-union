import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, FormattedDate, FormattedTime, injectIntl, intlShape } from 'react-intl';
import { connect } from 'react-redux';
import { showRouteSectionsChanged } from '../../RouteDetail/actions';
import renderRide from '../../../components/Ride';

function renderCar(car, setId) {
	return <span key={`${setId}-${car.id}-${car.description}`}>{car.name}</span>;
}

function renderLocomotive(train, setId) {
	return <span key={`${setId}-${train.id}`}>{train.name}</span>;
}

class Route extends React.Component {
	handleShowRouteSections = () => {
		console.log(this.props.route.showRouteSections);
		this.props.dispatch(showRouteSectionsChanged(this.props.route));
	};

	render() {
		const { intl } = this.props;

		const routeId = this.props.route.id;
		const locomotives = [];
		if (this.props.route.rides != null && this.props.route.rides.length > 0) {
			this.props.route.rides[0].locomotives.forEach(function(locomotive) {
				locomotives.push(renderLocomotive(locomotive, routeId));
			});
		}

		const cars = [];

		if (this.props.route.rides != null && this.props.route.rides.length > 0) {
			this.props.route.rides[0].cars.forEach(function(car) {
				cars.push(renderCar(car, routeId));
			});
		}

		const rides = [];

		if (this.props.route.rides != null && this.props.route.rides.length > 0) {
			this.props.route.rides.forEach(function(ride) {
				rides.push(renderRide(`${routeId}-${ride.id}`, ride));
			});
		}

		return (
			<tbody>
				<tr key={this.props.route.id} className="trainRow">
					<td>{this.props.route.name}</td>
					<td>{this.props.route.from.location}</td>
					<td>{this.props.route.to.location}</td>
					<td>
						{cars.length > 0 ? (
							cars.map(t => t).reduce((prev, curr) => [prev, ', ', curr])
						) : (
							<FormattedMessage id="trainsetDetail.notHaveConfiguration" />
						)}
					</td>
					<td>
						{locomotives.length > 0 ? (
							locomotives.map(t => t).reduce((prev, curr) => [prev, ', ', curr])
						) : (
							<FormattedMessage id="trainsetDetail.notHaveConfiguration" />
						)}
					</td>
					<td>
						{this.props.route.driver != null ? (
							`${this.props.route.driver.firstname} ${this.props.route.driver.lastname}`
						) : (
							<FormattedMessage id="trainsetDetail.notAssigned" />
						)}
					</td>
					<td>{this.props.route.driver != null ? this.props.route.driver.phoneNumber : ''}</td>
					<td />
					<td>
						<FormattedDate value={this.props.route.departureTime} />{' '}
						<FormattedTime value={this.props.route.departureTime} />
					</td>
					<td />
					<td>
						<FormattedDate value={this.props.route.arrivalTime} />{' '}
						<FormattedTime value={this.props.route.arrivalTime} />
					</td>
					<td>
						<div className="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
							<div className="btn-group mr-2" role="group" aria-label="First group">
								<button
									className="btn btn-info"
									onClick={() => this.handleShowRouteSections(this.props.route.id)}
								>
									{intl.formatMessage({ id: 'trainsetDetail.table.action.showRouteSections' })}
								</button>
							</div>
							<div className="btn-group mr-2" role="group" aria-label="First group" />
						</div>
					</td>
				</tr>
			</tbody>
		);
	}
}

Route.propTypes = {
	dispatch: PropTypes.func.isRequired,
	intl: intlShape.isRequired,
	route: PropTypes.shape({
		from: PropTypes.shape({
			location: PropTypes.string,
		}),
		name: PropTypes.string,
		to: PropTypes.shape({
			location: PropTypes.string,
		}),
		arrivalTime: PropTypes.string,
		departureTime: PropTypes.string,
		trains: PropTypes.arrayOf(
			PropTypes.shape({
				description: PropTypes.string,
				arrivalTime: PropTypes.instanceOf(Date),
				departureTime: PropTypes.instanceOf(Date),
			})
		),
		cars: PropTypes.arrayOf(
			PropTypes.shape({
				name: PropTypes.string,
				description: PropTypes.string,
			})
		),
		id: PropTypes.number,
		showRouteSections: PropTypes.bool,
		rides: PropTypes.array,
		driver: PropTypes.shape({
			id: PropTypes.number,
			firstname: PropTypes.string,
			lastname: PropTypes.string,
			phoneNumber: PropTypes.string,
		}),
		route: PropTypes.shape({
			from: PropTypes.shape({
				location: PropTypes.string,
			}),
			name: PropTypes.string,
			to: PropTypes.shape({
				location: PropTypes.string,
			}),
			departureTime: PropTypes.string,
			arrivalTime: PropTypes.string,
		}),
	}),
};

const mapStateToProps = () => {
	return {};
};

export default connect(mapStateToProps)(injectIntl(Route));
