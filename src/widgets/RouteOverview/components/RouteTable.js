/* eslint-disable react/prefer-stateless-function */
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import Route from './Route';

const React = require('react');

class RouteTable extends React.Component {
	render() {
		const rows = [];
		Array.prototype.forEach.call(this.props.routes, route => {
			rows.push(<Route key={route.id} route={route} />);
		});

		return (
			<div className="container">
				<table className="table table-striped">
					<thead>
						<tr>
							<th>
								<FormattedMessage id="label.route.name" />
							</th>
							<th>
								<FormattedMessage id="trainsetDetail.stationOfDeparture" />
							</th>
							<th>
								<FormattedMessage id="trainsetDetail.stationOfArrival" />
							</th>
							<th>
								<FormattedMessage id="trainsetDetail.cars" />
							</th>
							<th>
								<FormattedMessage id="trainsetDetail.trains" />
							</th>
							<th>
								<FormattedMessage id="trainsetDetail.driver" />
							</th>
							<th>
								<FormattedMessage id="trainsetDetail.phoneNumber" />
							</th>
							<th>
								<FormattedMessage id="trainsetDetail.timeOfDeparture" />
							</th>
							<th>
								<FormattedMessage id="trainsetDetail.timeOfArrival" />
							</th>
							<th>
								<FormattedMessage id="trainsetDetail.actions" />
							</th>
						</tr>
					</thead>
					{rows}
				</table>
			</div>
		);
	}
}

RouteTable.propTypes = {
	routes: PropTypes.arrayOf(
		PropTypes.shape({
			description: PropTypes.string,
		})
	),
	showRoutes: PropTypes.func,
};

export default RouteTable;
