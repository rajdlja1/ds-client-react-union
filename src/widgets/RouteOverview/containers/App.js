import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage, FormattedTime } from 'react-intl';
import { fetchItemsIfNeeded } from '../actions';
import RouteTable from '../components/RouteTable';
import SearchForm from '../components/SearchForm';

class App extends Component {
	static propTypes = {
		dispatch: PropTypes.func.isRequired,
		isFetching: PropTypes.bool.isRequired,
		lastUpdated: PropTypes.number,
		routes: PropTypes.array.isRequired,
	};

	componentDidMount() {
		const { dispatch } = this.props;
		dispatch(fetchItemsIfNeeded());
	}

	render() {
		const { routes, isFetching, lastUpdated } = this.props;
		const isEmpty = routes.length === 0;
		return (
			<div className="container">
				<h2><FormattedMessage id="label.dashboard" /></h2>
				<h3><FormattedMessage id="label.routes" /></h3>
				<MuiThemeProvider>
					<SearchForm />
				</MuiThemeProvider>
				<p>
					{lastUpdated &&
					<span>
						<FormattedMessage id="label.lastUpdatedAt" /> <FormattedTime value={new Date(lastUpdated)} />.
					</span>}
				</p>
				{isEmpty ? (
					isFetching ? (
						<h2><FormattedMessage id="label.loading" />.</h2>
					) : (
						<h2><FormattedMessage id="label.empty" />.</h2>
					)
				) : (
					<div style={{ opacity: isFetching ? 0.5 : 1 }}>
						<RouteTable routes={routes} />
					</div>
				)}
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { filteredRoutes } = state;
	const { isFetching, lastUpdated, items: routes } = filteredRoutes.routes || {
		isFetching: true,
		items: [],
	};

	return {
		routes,
		isFetching,
		lastUpdated,
	};
};

export default connect(mapStateToProps)(App);
